
# Q-One Assignment

To test this assignment you need to have `node` version 16 or more installed. It is also recommended to have docker but it's not a requirement.

There are 3 pre-created users: 

|Username|Password|     
|----|-----|  
|test1|123456|
|test2|123456|
|test3|123456|

You can use these 3 credentials to login and create items to test the application.





## Run Locally

Run the following command
```bash
  docker-compose up --build
```

If you don't have docker follow these steps:
```bash
  cd server && npm install && npm start
```

```bash
  cd client && npm install && npm start
```

## API Reference
You can also use this API to create your own users if you want.
#### Create Users

```http
  POST /user
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `username` | `string` | **Required**. Your username |
| `password` | `string` | **Required**. Your password  |


Takes username and password and creates a user.
