import React, { useEffect, useState } from "react";
import { getItemsList } from "../API";
import { constants } from "../constants";
import useModal from "../hooks/Modal.hook";
import {
  Button,
  ButtonContainer,
  ButtonHollow,
  Container,
  EditAndDeleteButton,
  Table,
  TD,
  TH,
  _LoginButton,
} from "../styled-components";
import { formatDate } from "../utils";
import DeleteItem from "./DeleteItem";
import EditItem from "./EditItem";
import Modal from "./Modal";

const ItemList = ({ setStepCount }) => {
  const [itemList, setItemList] = useState([]);
  const [itemToBeEditedOrDeleted, setItemToBeEditedOrDeleted] = useState();

  const getItems = async () => {
    getItemsList().then((response) => {
      if (response.status) {
        setItemList(response.data);
      }
    });
  };
  useEffect(() => {
    getItems();
  }, []);

  const { isShowing: isEditShowing, toggle: toggleEdit } = useModal();
  const { isShowing: isDeleteShowing, toggle: toggleDelete } = useModal();
  return (
    <Container>
      <Modal
        color={constants.theme.primaryColor}
        title="Edit Item"
        isShowing={isEditShowing}
        hide={toggleEdit}
      >
        <EditItem
          _id={itemToBeEditedOrDeleted}
          toggle={toggleEdit}
          cb={getItems}
        />
      </Modal>

      <Modal
        color={constants.theme.secondaryColor}
        title="Warning"
        isShowing={isDeleteShowing}
        hide={toggleDelete}
      >
        <DeleteItem
          _id={itemToBeEditedOrDeleted}
          toggle={toggleDelete}
          cb={getItems}
        />
      </Modal>
      <Table>
        <tbody>
          <tr>
            <TH>Item Name</TH>
            <TH>User Added</TH>
            <TH>Date Added</TH>
            <TH>State</TH>
            <TH>Action</TH>
          </tr>
          {itemList.length > 0 &&
            itemList.map((item) => (
              <tr key={item._id}>
                <TD>{item.itemName}</TD>
                <TD>{item.addedBy}</TD>
                <TD>{formatDate(item.dateAdded)}</TD>
                <TD>
                  {item.state === constants.active ? "Active" : "Not Active"}
                </TD>
                <TD>
                  <EditAndDeleteButton
                    padding="5px 0 0 0"
                    color="blue"
                    onClick={() => {
                      toggleEdit();
                      setItemToBeEditedOrDeleted(item._id);
                    }}
                  >
                    Edit
                  </EditAndDeleteButton>
                  <EditAndDeleteButton
                    color="red"
                    onClick={() => {
                      toggleDelete();
                      setItemToBeEditedOrDeleted(item._id);
                    }}
                  >
                    Delete
                  </EditAndDeleteButton>
                </TD>
              </tr>
            ))}
        </tbody>
      </Table>

      <ButtonContainer>
        <ButtonHollow
          onClick={() => {
            setStepCount(1);
          }}
        >
          Add more items
        </ButtonHollow>
        <Button
          onClick={() => {
            setStepCount(0);
            localStorage.removeItem("TOKEN");
          }}
        >
          Log Out
        </Button>
      </ButtonContainer>
    </Container>
  );
};

export default ItemList;
