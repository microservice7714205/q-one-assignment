import React, { useState } from "react";
import { Field, Form, Formik, useFormik } from "formik";
import { constants } from "../constants";
import { addItem, login } from "../API";
import { useDispatch } from "react-redux";
import { itemIdAction } from "../store/item-slice";
import { itemSchema } from "../yupValidation";
import {
  Container,
  FormStyle,
  LoginInput,
  AddItemSelect,
  _Form,
  Label,
  Button,
  ButtonContainer,
  ErrorMessage,
} from "../styled-components";

const AddItem = ({ setStepCount }) => {
  const dispatch = useDispatch();

  const [itemState, setItemState] = useState("");
  const [errorResponseMessage, setErrorResponseMessage] = useState("");
  return (
    <Container>
      <FormStyle
        initialValues={{ itemName: "", state: itemState }}
        validationSchema={itemSchema}
        onSubmit={(values) => {
          addItem({ ...values, state: itemState }).then((response) => {
            if (response.status) {
              dispatch(itemIdAction(response.data.insertedId));
              setStepCount(2);
            } else {
              setErrorResponseMessage(response.message);
            }
          });
        }}
        onChange={(e) => console.log}
      >
        {({ errors, touched }) => (
          <_Form>
            <Label>Enter Name</Label>
            <LoginInput
              type="itemName"
              name="itemName"
              placeholder="Enter Name"
            />
            {errors.itemName && touched.itemName ? (
              <ErrorMessage>{errors.itemName}</ErrorMessage>
            ) : null}
            <Label>Select State</Label>
            <AddItemSelect
              as="select"
              name="state"
              onChange={(e) => {
                setItemState(e.target.value);
              }}
            >
              <option value="">Select Value</option>
              <option key={constants.active} value={constants.active}>
                Active
              </option>
              <option key={constants.notActive} value={constants.notActive}>
                Not Active
              </option>
            </AddItemSelect>
            {errors.state && touched.state ? (
              <ErrorMessage>{errors.state}</ErrorMessage>
            ) : null}
            {errorResponseMessage && (
              <ErrorMessage>{errorResponseMessage}</ErrorMessage>
            )}
            <ButtonContainer justifyContent="center">
              <Button type="submit">Submit</Button>
            </ButtonContainer>{" "}
          </_Form>
        )}
      </FormStyle>
    </Container>
  );
};

export default AddItem;
