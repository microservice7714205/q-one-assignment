import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { getItemById } from "../API";
import { constants } from "../constants";
import {
  Button,
  ButtonContainer,
  ButtonHollow,
  Item,
  ItemContainer,
  ItemData,
  ItemLabel,
} from "../styled-components";
import { formatDate } from "../utils";

const PreviewItem = ({ setStepCount }) => {
  const itemId = useSelector((state) => state.item.itemId);
  const [itemDetails, setItemDetails] = useState(null);

  useEffect(() => {
    if (itemId) {
      getItemById(itemId).then((response) => {
        if (response.status) {
          setItemDetails(response.data);
        }
      });
    }
  }, [itemId]);

  return (
    <ItemContainer>
      {itemDetails && (
        <>
          <Item>
            <ItemLabel>Item Name</ItemLabel>
            <ItemData>{itemDetails?.itemName}</ItemData>
          </Item>
          <Item>
            <ItemLabel>User Added</ItemLabel>
            <ItemData>{itemDetails?.addedBy}</ItemData>
          </Item>
          <Item>
            <ItemLabel>Date Added</ItemLabel>
            <ItemData>{formatDate(itemDetails?.dateAdded)}</ItemData>
          </Item>
          <Item>
            <ItemLabel>State</ItemLabel>
            <ItemData>
              {itemDetails.state === constants.active ? "Active" : "Not Active"}
            </ItemData>
          </Item>
        </>
      )}

      <ButtonContainer justifyContent="none">
        <ButtonHollow
          onClick={() => {
            setStepCount(1);
          }}
        >
          Add More Items
        </ButtonHollow>
        <Button
          onClick={() => {
            setStepCount(3);
          }}
          minWidth="150px"
        >
          Submit
        </Button>
      </ButtonContainer>
    </ItemContainer>
  );
};

export default PreviewItem;
