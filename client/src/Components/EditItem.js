import React, { useEffect, useState } from "react";
import { Field, Form, Formik } from "formik";
import { constants } from "../constants";
import { editItem, getItemById } from "../API";
import { itemIdAction } from "../store/item-slice";
import { itemSchema } from "../yupValidation";
import { useDispatch } from "react-redux";
import {
  Container,
  FormStyle,
  Label,
  LoginInput,
  AddItemSelect,
  _Form,
  ButtonContainer,
  Button,
  ButtonHollow,
  ErrorMessage,
} from "../styled-components";

const EditItem = ({ _id, toggle, cb }) => {
  const [itemDetails, setItemDetails] = useState({ itemName: "", state: "" });
  const [errorResponseMessage, setErrorResponseMessage] = useState("");

  useEffect(() => {
    getItemById(_id).then((response) => {
      if (response.status) {
        setItemDetails(response.data);
        setItemState(response.data.state);
      } else {
        alert(response.message);
      }
    });
  }, []);

  const [itemState, setItemState] = useState("");

  return (
    <Container>
      <FormStyle
        initialValues={{
          itemName: itemDetails?.itemName,
          state: itemDetails?.state,
        }}
        enableReinitialize={true}
        validationSchema={itemSchema}
        onSubmit={(updatedValues) => {
          editItem(_id, { ...updatedValues, state: itemState }).then(
            (response) => {
              if (response.status) {
                alert(response.message);
                toggle(cb);
              } else {
                // alert(response.message);
                setErrorResponseMessage(response.message);
              }
            }
          );
        }}
      >
        {({ errors, touched }) => (
          <_Form width="100%">
            <Label>Enter Name</Label>
            <LoginInput
              type="itemName"
              name="itemName"
              placeholder="Enter Name"
            />
            {errors.itemName && touched.itemName ? (
              <ErrorMessage>{errors.itemName}</ErrorMessage>
            ) : null}
            <Label>Select State</Label>
            <AddItemSelect
              as="select"
              name="state"
              value={itemState}
              onChange={(e) => {
                setItemState(e.target.value);
              }}
            >
              <option value={constants.active}>Active</option>
              <option value={constants.notActive}>Not Active</option>
            </AddItemSelect>
            {errors.state && touched.state ? (
              <ErrorMessage>{errors.state}</ErrorMessage>
            ) : null}

            {errorResponseMessage && (
              <ErrorMessage>{errorResponseMessage}</ErrorMessage>
            )}
            <ButtonContainer justifyContent="end">
              <ButtonHollow onClick={() => toggle()}>Cancel</ButtonHollow>
              <Button type="submit">Save</Button>
            </ButtonContainer>
          </_Form>
        )}
      </FormStyle>
    </Container>
  );
};

export default EditItem;
