import React from "react";
import { constants } from "../constants";
import {
  ModalOverlay,
  ModalTitle,
  ModalWrapper,
  _Modal,
} from "../styled-components";
const Modal = (props) => {
  return (
    <>
      {props.isShowing ? (
        <>
          <ModalOverlay />
          <ModalWrapper aria-modal aria-hidden tabIndex={-1} role="dialog">
            <_Modal>
              <ModalTitle bgColor={props.color}>{props.title}</ModalTitle>
              {props.children}
            </_Modal>
          </ModalWrapper>
        </>
      ) : null}
    </>
  );
};
export default Modal;
