import React from "react";
import { login } from "../API";
import { loginSchema } from "../yupValidation";
import {
  Container,
  FormStyle,
  _Form,
  Label,
  LoginInput,
  _LoginButton,
  LoginButton,
  Button,
} from "../styled-components";

const LoginForm = ({ setStepCount }) => {
  return (
    <Container>
      <FormStyle
        initialValues={{
          username: "",
          password: "",
        }}
        validationSchema={loginSchema}
        onSubmit={(values) => {
          login(values)
            .then((response) => {
              if (response.status) {
                localStorage.setItem("TOKEN", response.data);
                setStepCount(1);
              } else {
                alert(response.message);
              }
            })
            .catch((error) => console.log);
        }}
      >
        {({ errors, touched }) => (
          <_Form>
            <Label color="grey">Enter User Name</Label>
            <LoginInput
              name="username"
              type="text"
              placeholder="Enter User Name"
            />
            {errors.username && touched.username ? (
              <div>{errors.username}</div>
            ) : null}

            <Label color="grey">Enter Password</Label>
            <LoginInput
              name="password"
              type="password"
              placeholder="Enter Password"
            />
            {errors.password && touched.password ? (
              <div>{errors.password}</div>
            ) : null}
            <_LoginButton>
              <Button type="submit">Log In</Button>
            </_LoginButton>
          </_Form>
        )}
      </FormStyle>
    </Container>
  );
};

export default LoginForm;
