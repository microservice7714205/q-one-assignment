import React, { useEffect, useState } from "react";
import { deleteItemById } from "../API";
import { constants } from "../constants";
import {
  Button,
  ButtonContainer,
  ButtonHollow,
  Container,
  Label,
} from "../styled-components";

const DeleteItem = ({ _id, toggle, cb }) => {
  const deleteItem = () => {
    deleteItemById(_id).then((response) => {
      if (response.status) {
        toggle(cb);
        alert(response.message);
      } else {
        alert(response.message);
      }
    });
  };
  return (
    <Container>
      <Label color={constants.theme.secondaryColor}>Are you sure?</Label>
      <ButtonContainer>
        <ButtonHollow
          color={constants.theme.secondaryColor}
          onClick={() => toggle()}
        >
          No, keep
        </ButtonHollow>
        <Button
          backgroundColor={constants.theme.secondaryColor}
          onClick={deleteItem}
        >
          Yes, delete
        </Button>
      </ButtonContainer>
    </Container>
  );
};

export default DeleteItem;
