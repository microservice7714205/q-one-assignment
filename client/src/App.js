import logo from "./logo.svg";
import "./App.css";
import LoginForm from "./Components/Login";
import AddItem from "./Components/AddItem";
import PreviewItem from "./Components/PreviewItem";
import ItemList from "./Components/ItemList";
import { useEffect, useState } from "react";

import { Modules, ModulesName, Steps, Title } from "./styled-components";

function App() {
  const [stepCount, setStepCount] = useState();
  const [step, setStep] = useState(null);

  useEffect(() => {
    localStorage.getItem("TOKEN") ? setStepCount(1) : setStepCount(0);
  }, []);

  useEffect(() => {
    switch (stepCount) {
      case 0: {
        setStep(<LoginForm setStepCount={setStepCount} />);
        break;
      }

      case 1: {
        setStep(<AddItem setStepCount={setStepCount} />);
        break;
      }

      case 2: {
        setStep(<PreviewItem setStepCount={setStepCount} />);
        break;
      }

      case 3: {
        setStep(<ItemList setStepCount={setStepCount} />);
      }
    }
  }, [stepCount]);

  return (
    <div>
      <Modules>
        <ModulesName active={stepCount >= 0}>
          <Steps>Step 1</Steps>
          <Title>Log in user</Title>
        </ModulesName>
        <ModulesName active={stepCount >= 1}>
          <Steps>Step 2</Steps>
          <Title>Add item</Title>
        </ModulesName>
        <ModulesName active={stepCount >= 2}>
          <Steps>Step 3</Steps>
          <Title>Preview Item</Title>
        </ModulesName>
        <ModulesName active={stepCount >= 3}>
          <Steps>Step 4</Steps>
          <Title>Edit item</Title>
        </ModulesName>
      </Modules>

      {step}
    </div>
  );
}

export default App;
