import { constants } from "../constants";
import { baseURL, customFetch } from "./apiUtils";

let QOneFetch = new customFetch();
QOneFetch = QOneFetch.create({
  baseURL: baseURL,
  headers: {
    "Content-Type": "application/json",
  },
});

QOneFetch.interceptor.request = (req) => {
  let token = localStorage.getItem("TOKEN");
  let headers = req.headers;
  if (token) {
    headers.append("authorization", token);
  }
  return req;
};

export const addItem = async (itemInfo) => {
  let response = QOneFetch.post(constants.API.item.addItem, itemInfo);
  return response;
};

export const login = async (loginDetails) => {
  let response = QOneFetch.post(constants.API.user.login, loginDetails);
  return response;
};

export const getItemById = async (itemId) => {
  let response = QOneFetch.get(`${constants.API.item.getItemById}/${itemId}`);
  return response;
};

export const getItemsList = async () => {
  let response = QOneFetch.get(`${constants.API.item.getItemsList}`);
  return response;
};

export const editItem = async (itemId, updatedItem) => {
  let response = QOneFetch.put(
    `${constants.API.item.editItem}/${itemId}`,
    updatedItem
  );
  return response;
};

export const deleteItemById = async (itemId) => {
  let response = QOneFetch.delete(`${constants.API.item.deleteItem}/${itemId}`);
  return response;
};
