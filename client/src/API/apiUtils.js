export class customFetch {
  headers = new Headers();
  baseURL;

  constructor() {}

  interceptor = {
    request: (req) => req,
    response: (res) => res,
  };

  create(options) {
    const { baseURL, headers } = options;

    // Set base url
    this.baseURL = baseURL;

    // Parse over headers passed
    for (const key in headers) {
      if (Object.hasOwnProperty.call(headers, key)) {
        this.headers.append(key, headers[key]);
      }
    }
    return this;
  }

  generateRequest = (endpoint, options) => {
    let req = new Request(`${this.baseURL}${endpoint}`, options);
    return req;
  };

  async get(endpoint) {
    let request = this.generateRequest(endpoint, {
      method: "GET",
      headers: this.headers,
    });

    request = this.interceptor.request(request);
    return fetch(request)
      .then((response) => preResponseHandler(response))
      .then((response) => response.json())
      .then((response) => this.interceptor.response(response))
      .then((response) => handleResponse(response));
  }

  async post(endpoint, body) {
    let request = this.generateRequest(endpoint, {
      method: "POST",
      headers: this.headers,
      body: JSON.stringify(body),
    });

    request = this.interceptor.request(request);

    return fetch(request)
      .then((response) => preResponseHandler(response))
      .then((response) => response.json())
      .then((response) => this.interceptor.response(response))
      .then((response) => handleResponse(response))
      .catch((error) => handleError(error));
  }

  async put(endpoint, body) {
    let request = this.generateRequest(endpoint, {
      method: "PUT",
      headers: this.headers,
      body: JSON.stringify(body),
    });

    request = this.interceptor.request(request);

    return fetch(request)
      .then((response) => preResponseHandler(response))
      .then((response) => response.json())
      .then((response) => this.interceptor.response(response))
      .then((response) => handleResponse(response));
  }

  async delete(endpoint, body) {
    let request = this.generateRequest(endpoint, {
      method: "DELETE",
      headers: this.headers,
      body: JSON.stringify(body),
    });

    request = this.interceptor.request(request);

    return fetch(request)
      .then((response) => preResponseHandler(response))
      .then((response) => response.json())
      .then((response) => this.interceptor.response(response))
      .then((response) => handleResponse(response));
  }
}

export const baseURL = `http://localhost:9870/`;
// export const baseURL = "http://server/"

export const handleResponse = (response) => {
  return {
    status: response.success,
    data: response.data,
    message: response.message,
    ...response,
  };
};

export const handleError = (error) => error;

export const preResponseHandler = (response) => {
  if (response.status === 401) {
    return Promise.reject({
      status: 0,
      errorCode: 401,
      message: "Unauthorized",
    });
  }
  return response;
};
