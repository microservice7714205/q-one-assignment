import styled from "styled-components";
import { Field, Form, Formik, useFormik } from "formik";
import { constants } from "./constants";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const FormStyle = styled(Formik)`
  display: flex;
  flex-direction: column;
  padding: 20px;
  background-color: #fff;
  border-radius: 5px;
  width: 50%;
`;

export const Label = styled.label`
  margin-bottom: 10px;
  margin-top: 10px;
  font-weight: 600;
  // opacity: 0.5;
  color: ${(props) => props.color ?? "grey"};
`;

export const LoginInput = styled(Field)`
  margin-bottom: 10px;
  padding: 10px;
  border: solid #989898;
  border-radius: 8px;
  opacity: 0.5;
  font-size: 14px;
`;

export const _LoginButton = styled.div`
  text-align: center;
`;

export const LoginButton = styled.button`
  width: 18%;
  padding: 15px;
  margin-top: 50px;
  color: white;
  border: none;
  border-radius: 8px;
  font-size: 16px;
  cursor: pointer;
  background-color:${constants.theme.primaryColor}
  font-weight: 600;
`;

export const _Form = styled(Form)`
  display: flex;
  flex-direction: column;
  padding: 20px;
  background-color: #fff;
  border-radius: 5px;
  width: ${(props) => props.width ?? "50%"};
`;

export const ItemContainer = styled.div`
  margin: auto;
  align-items: center;
  width: 27%;
`;

export const Item = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 25px;
`;
export const ItemLabel = styled.label`
  font-weight: 500;
  width: 50%;
`;

export const ItemData = styled.label`
  width: 50%;
`;

export const ButtonContainer = styled.div`
  display: flex;
  justify-content: ${(props) => props.justifyContent ?? "space-between"};
  margin-top: 25px;
  min-width: 250px;
`;

export const ButtonHollow = styled.button`
  padding: 15px;
  margin-top: 30px;
  color: ${(props) => props.color ?? constants.theme.primaryColor};
  border-radius: 8px;
  background: transparent;
  font-size: 14px;
  cursor: pointer;
  border: 1px solid ${(props) => props.color ?? constants.theme.primaryColor};
  text-color: ${(props) => props.color ?? constants.theme.primaryColor};
  font-weight: 600;
  outline: none;
`;
export const Button = styled.button`
  // width: 30%;
  padding: 15px;
  margin-top: 30px;
  color: white;
  border: none;
  border-radius: 8px;
  font-size: 14px;
  cursor: pointer;
  background-color: ${(props) =>
    props.backgroundColor ?? constants.theme.primaryColor};

  ${(props) => `min-width: ${props.minWidth}`};
  font-weight: 600;
  margin-left: 15px;
`;

export const AddItemSelect = styled(Field)`
  margin-bottom: 10px;
  padding: 10px;
  border: solid #989898;
  border-radius: 8px;
  opacity: 0.5;
`;

export const EditAndDeleteButton = styled.span`
  display: inline-block;
  color: ${(props) => props.color};
  text-decoration: underline;
  padding: 0 15px 0 0;
`;

export const ModalTitle = styled.div`
  background-color: ${(props) => props.bgColor};
  border-radius: 3px 3px 0px 0px;
  margin-left: -32px;
  margin-right: -32px;
  margin-top: -32px;
  margin-bottom: 20px;
  padding: 15px;
  color: #fff;
`;

export const ModalOverlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1040;
  width: 100vw;
  height: 100vh;
  background-color: #000;
  opacity: 0.5;
`;

export const ModalWrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1050;
  width: 100%;
  height: 100%;
  overflow-x: hidden;
  overflow-y: auto;
  outline: 0;
`;

export const _Modal = styled.div`
  z-index: 100;
  background: white;
  position: relative;
  margin: 1.75rem auto;
  border-radius: 3px;
  max-width: 500px;
  padding: 2rem;
`;

export const Table = styled.table`
  width: calc(100% - 80px);
  margin: 0 auto;
  border-collapse: collapse;
  text-align: left;
`;

export const TH = styled.th`
  border: 2px solid #ff7250;
  padding: 1rem;
`;

export const TD = styled.td`
  border: 2px solid #ff7250;
  padding: 0.5rem 1rem;
`;

export const ErrorMessage = styled.span`
  color: red;
`;

export const Modules = styled.ul`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  margin-top: 50px;
  list-style-type: none;
  font-color: #ff7250;
  margin-right: 40px;
  margin-bottom: 0px;
`;

export const ModulesName = styled.li`
  cursor: pointer;
  align-items: center;
  text-align: center;
  padding: 25px;
  border: 1px solid #ff7250;
  width: 100%;
  color: #ff7250;
  background-color: white;
  ${(props) => props.active && `background-color: #ff7250;color: white;`}
`;
export const Steps = styled.div`
  font-size: 20px;
  font-weight: 600;
`;

export const Title = styled.div`
  font-size: 14px;
  margin-top: 10px;

  font-weight: 400;
`;
