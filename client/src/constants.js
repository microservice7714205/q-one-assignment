export const constants = {
  API: {
    user: {
      login: "user/login",
    },
    item: {
      addItem: "item/add",
      getItemById: "item",
      getItemsList: "item",
      editItem: "item",
      deleteItem: "item",
    },
  },
  active: "ACTIVE",
  notActive: "NOT_ACTIVE",
  theme: {
    primaryColor: "#ff7250",
    secondaryColor: "#992a27",
  },
  ITEM_NAME_REQUIRED: "Item name cannot be empty",
  USERNAME_REQUIRED: "Username is required",
  PASSWORD_REQUIRED: "Password is required",
};
