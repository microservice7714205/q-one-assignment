import { useState } from "react";

const useModal = () => {
  const [isShowing, setIsShowing] = useState(false);

  function toggle(cb = () => {}) {
    setIsShowing(!isShowing);
    cb();
  }

  return {
    isShowing,
    toggle,
  };
};

export default useModal;
