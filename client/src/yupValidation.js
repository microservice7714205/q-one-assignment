import * as Yup from "yup";
import { constants } from "./constants";

export const itemSchema = Yup.object().shape({
  itemName: Yup.string()
    .min(4, "Too Short!")
    .required(constants.ITEM_NAME_REQUIRED),
  state: Yup.string().oneOf([constants.active, constants.notActive]),
});

export const loginSchema = Yup.object().shape({
  username: Yup.string()
    .min(2, "Too Short!")
    .required(constants.USERNAME_REQUIRED),
  password: Yup.string()
    .min(2, "Too Short!")
    .required(constants.PASSWORD_REQUIRED),
});
