import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  itemId: "",
};

export const itemReducer = createSlice({
  name: "item",
  initialState,
  reducers: {
    itemIdAction: (state, action) => ({
      ...state,
      itemId: action.payload,
    }),
  },
});

export const { itemIdAction } = itemReducer.actions;

export default itemReducer.reducer;
