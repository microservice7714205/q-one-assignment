import { applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import { configureStore } from "@reduxjs/toolkit";

// import userReducer from "./user-slice";
import  itemReducer from "./item-slice";

export const store = configureStore(
  {
    reducer: {
      // user: userReducer,
      item: itemReducer,
    },
  },
  applyMiddleware(thunkMiddleware)
);

export const appDispatch = () => store.dispatch;
