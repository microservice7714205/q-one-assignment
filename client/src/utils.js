/**
 *
 * @param {String} date date in the form of string
 * returns string in the format of DD-MM-YYYY HH:MM
 */
export const formatDate = (date) => {
  const dateObj = new Date(date);
  const year = dateObj.getUTCFullYear();

  let month = dateObj.getUTCMonth() + 1;
  month = month < 10 ? `0${month}` : month;

  let day = dateObj.getUTCDate();
  day = day < 10 ? `0${day}` : day;

  let hour = dateObj.getHours();
  hour = hour < 10 ? `0${hour}` : hour;

  let minutes = dateObj.getMinutes();
  minutes = minutes < 10 ? `0${minutes}` : minutes;

  return `${day}-${month}-${year} ${hour}:${minutes}`;
};
