const respondSuccess = (res, message, data) => {
  if (!data) {
    return res.status(200).json({
      success: true,
      message: !message ? querySuccesful : message,
    });
  }

  return res.status(200).json({
    success: true,
    message: !message ? querySuccesful : message,
    data,
  });
};

const respondFailure = (res, message, statusCode) => {
  let statusData = statusCode || 200;

  return res.status(statusData).json({
    success: false,
    message: !message ? somethingWentWrong : message,
  });
};

module.exports = { respondSuccess, respondFailure };
