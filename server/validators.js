const { Validator } = require("node-input-validator");
const { constants } = require("./constants");

const userValidator = async (user) => {
  const validator = new Validator(user, {
    username: "required",
    password: "required",
  });

  return validator.check();
};

const itemValidator = async (item) => {
  const validator = new Validator(item, {
    name: "required",
    state: `required|in:${constants.state.join(",")}`,
  });

  return validator.check();
};

const stateValidator = async (state) => {
  const validator = new Validator(
    { state },
    {
      state: `required|in:${constants.state.join(",")}`,
    }
  );

  return validator.check();
};

module.exports = { userValidator, itemValidator, stateValidator };
