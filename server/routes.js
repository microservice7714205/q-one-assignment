const { requireAuth } = require("./passport-auth");
const { itemRouter } = require("./Routes/item.route");
const { userRouter } = require("./Routes/user.route");
const router = require("express").Router();

router.use("/user", userRouter);
router.use("/item", requireAuth, itemRouter);

module.exports = { router };
