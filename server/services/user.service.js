const { User } = require("../Models/user.model");

/**
 *
 * @param {Object | Array<Object>} userInfo could either be an Object or an Array of Object
 *
 * This function shall be used to create dummy accounts so that user can login
 */
const createUser = async (userInfo) => {
  // If user info is array parse it accordingly
  if (Array.isArray(userInfo)) {
    return User.insertMany(userInfo);
  }

  // Otherwise parse it as an Object
  return User.save(userInfo);
};

/**
 *
 * @param {string} username Entered username to be checked against database
 * @param {string} password Entered password to be checked against the user
 *
 * Used for login
 */
const login = async (username, password) => {};

/**
 *
 * @param {Object} query general object where one can pass the query to get data from databasr
 * returns Object
 */
const getUser = async (query = {}) => {
  let userData = await User.findOne(query);
  return userData;
};

module.exports = { getUser, createUser };
