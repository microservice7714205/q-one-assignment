const { ObjectId } = require("mongodb");
const { Item } = require("../Models/item.model");

/**
 *
 * @param {Object | Array<Object>} itemInfo could either be an Object or an Array of Object
 *
 * Used to create and item
 */
const createItem = async (itemInfo) => {
  if (Array.isArray(itemInfo)) {
    return Item.insertMany(itemInfo);
  }
  return Item.save(itemInfo);
};

/**
 *
 * @param {string} itemId Id of the item we are getting
 *
 * Returns a single item using the item Id
 */
const getItemById = async (itemId) =>
  Item.findOne({ _id: new ObjectId(itemId) });

/**
 * Returns list of all item. There is not pagination as of yet so it returns all the items in an array
 */
const getItemList = async () => Item.find({}).toArray();

/**
 *
 * @param {Object} query filtering query to get records which needs to be updated
 * @param {Object} update actual update on the record(s)
 * @returns updated item(s)
 * query and update are null because if anyone uses this function without providing those parameters
 * that will throw an error
 */
const updateItem = async (query = null, update = null) => {
  if (!(query || update)) {
    return Promise.reject("Please provide query");
  }
  let updatedItem = await Item.findOneAndUpdate(query, update);
  return updatedItem;
};

/**
 *
 * @param {String} itemId Id of the item to be deleted
 * @returns
 */
const deleteItemById = async (itemId) => {
  return Item.findOneAndDelete({ _id: new ObjectId(itemId) });
};

/**
 *
 * @param {Object} query Based on the query checks for the existence of the item
 * @returns Boolean
 */
const checkExistenceOfItem = async (query) => {
  let exist = await Item.findOne(query);
  return exist ? true : false;
};

module.exports = {
  createItem,
  getItemById,
  getItemList,
  updateItem,
  deleteItemById,
  checkExistenceOfItem,
};
