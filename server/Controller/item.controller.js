const { ObjectId } = require("mongodb");
const { constants } = require("../constants");
const { respondSuccess, respondFailure } = require("../responseHandler");
const itemService = require("../services/item.service");
const { itemValidator, stateValidator } = require("../validators");

// Gets item by item Id
const getItemById = async (req, res) => {
  try {
    let { itemId } = req.params;
    let itemData = await itemService.getItemById(itemId);
    if (itemData) return respondSuccess(res, constants.ITEM_FETCHED, itemData);
    return respondFailure(res, {}, 500);
  } catch (error) {
    return respondFailure(res, error.message, 500);
  }
};

// Get lists of all items
const getItemList = async (req, res) => {
  try {
    let itemList = await itemService.getItemList();
    return respondSuccess(res, constants.ITEM_LIST_FETCHED, itemList);
  } catch (error) {
    return respondFailure(res, error.message, 500);
  }
};

// Create an item
const addItem = async (req, res) => {
  try {
    let { itemName, state } = req.body,
      { username } = req.user;

    const isItemValid = await itemValidator({ name: itemName, state });
    if (isItemValid) {
      let doesItemExist = await itemService.checkExistenceOfItem({ itemName });
      if (!doesItemExist) {
        let item = await itemService.createItem({
          itemName,
          state,
          addedBy: username,
          dateAdded: new Date(),
        });

        if (item) {
          return respondSuccess(res, constants.ITEM_ADDED, item);
        }

        return respondFailure(res, constants.ITEM_NOT_ADDED, 500);
      } else {
        return respondFailure(res, constants.ITEM_ALREADY_EXIST, 409);
      }
    }
    return respondFailure(res, constants.INVALID_REQUEST, 400);
  } catch (error) {
    return respondFailure(res, error.message, 500);
  }
};

// Update an item by item Id
const updateItem = async (req, res) => {
  try {
    let { itemName, state } = req.body,
      { itemId } = req.params;

    if (state) {
      let isStateValid = await stateValidator(state);
      if (!isStateValid) {
        throw new Error(
          `State can only be either of two value: ${constants.state.join(", ")}`
        );
      }
    }

    let doesItemExist = itemService.checkExistenceOfItem({
      _id: new ObjectId(itemId),
    });
    let isNameReservedForDifferentItem = itemService.checkExistenceOfItem({
      itemName,
      _id: { $ne: new ObjectId(itemId) },
    });

    [doesItemExist, isNameReservedForDifferentItem] = await Promise.all([
      doesItemExist,
      isNameReservedForDifferentItem,
    ]);

    switch (true) {
      case doesItemExist && !isNameReservedForDifferentItem: {
        let updatedItem = await itemService.updateItem(
          { _id: new ObjectId(itemId) },
          {
            $set: {
              ...(itemName && { itemName }),
              ...(state && { state }),
            },
          }
        );
        return respondSuccess(res, constants.ITEM_UPDATE_SUCCESS, updatedItem);
      }

      case !doesItemExist: {
        return respondFailure(res, constants.ITEM_NOT_EXIST, 404);
      }

      case isNameReservedForDifferentItem: {
        return respondFailure(res, constants.ITEM_ALREADY_EXIST, 409);
      }
    }
  } catch (error) {
    return respondFailure(res, error.message, 500);
  }
};

// Delete item by item id
const deleteItemById = async (req, res) => {
  try {
    let { itemId } = req.params;
    let item = await itemService.deleteItemById(itemId);
    return respondSuccess(res, constants.DELETE_SUCCESS, item);
  } catch (error) {
    return respondFailure(res, error.message, 500);
  }
};

module.exports = {
  getItemById,
  getItemList,
  addItem,
  updateItem,
  deleteItemById,
};
