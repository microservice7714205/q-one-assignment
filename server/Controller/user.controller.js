const { respondSuccess, respondFailure } = require("../responseHandler");
const { userValidator } = require("../validators");
const userService = require("../services/user.service");
const { constants } = require("../constants");
const { tokenForUser } = require("../helpers/jwt.helper");

// Creates a new user
const createuser = async (req, res) => {
  try {
    let { username, password } = req.body;
    let isValidUser = await userValidator({ username, password });
    if (isValidUser) {
      const createdUser = await userService.createUser({ username, password });
      return respondSuccess(res, constants.USER_CREATED, createdUser);
    }
    return respondFailure(res, constants.USER_DOESNT_EXIST);
  } catch (error) {
    return respondFailure(res, error.message, 400);
  }
};

// Check the credentials provided and returns a token if those are correct
const login = async (req, res) => {
  try {
    let { username, password } = req.body;

    let isValidLoginDetails = await userValidator({ username, password });
    if (isValidLoginDetails) {
      let userData = await userService.getUser({ username });
      if (userData) {
        if (userData.password === password) {
          const token = tokenForUser({
            _id: userData._id,
            username: userData.username,
          });
          return respondSuccess(res, constants.SUCCESS, token);
        } else {
          return respondFailure(res, constants.INCORRECT_PASSWORD);
        }
      }

      return respondFailure(res, constants.USER_DOESNT_EXIST);
    }
    return respondFailure(res, constants.INVALID_REQUEST, 500);
  } catch (error) {
    return respondFailure(res, error.message, 400);
  }
};

module.exports = { createuser, login };
