/**
 * Loading Environment variables
 */
const { getEnv } = require("./env-parser");
getEnv();

/**
 * Connect to DBu
 */
const { dbClient } = require("./dbconfig");
dbClient.connectToDB().then(async () => {
  /**
   * Initialize app
   */
  const express = require("express");
  const { router } = require("./routes");

  // Create multiple dummy objects if they don't exist
  const { createDummyUsersIfTheyDontExist } = require("./helpers/user.helper");
  createDummyUsersIfTheyDontExist();

  const app = express();
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));

  app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.setHeader(
      "Access-Control-Allow-Headers",
      "Content-Type, Authorization"
    );
    next();
  });

  app.options("/*", (_, res) => {
    res.sendStatus(200);
  });

  app.use("/", router);

  const port = process.env.SERVER_PORT || 8000;

  /**
   * Initialize server
   */
  app.listen(port, () => {
    console.log(`Listening on port ${port}`);
  });
});

// Validate each request with middleware
