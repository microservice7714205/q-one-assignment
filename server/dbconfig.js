const { MongoClient } = require("mongodb");
const { Modal } = require("./BaseDao");
const config = {
  dbName: process.env.MONGODB_DB_NAME,
  username: process.env.MONGODB_USERNAME,
  password: process.env.MONGODB_PASSWORD,
};

class dbClient {
  constructor() {}

  database;
  model;

  static async connectToDB() {
    const url = `mongodb+srv://${config.username}:${config.password}@q-one-tech.drgk2hz.mongodb.net`;
    // const url = "mongodb://localhost:27017"
    const client = new MongoClient(url);

    // Connect to database
    await client.connect();

    console.log("Connected successfully to database");
    this.database = client.db(config.dbName);

    return this.database;
  }

  static model(collectionName) {
    let model = new Modal(collectionName, this.database);
    return model;
  }
}

// module.exports = { connectToDB, dbClient: database };
module.exports = {
  dbClient,
};
