const constants = {
  state: ["ACTIVE", "NOT_ACTIVE"],
  user: "users",
  item: "items",
  ITEM_UPDATE_SUCCESS: "Item updated successfully",
  ITEM_NOT_EXIST: "Item doesn't exist",
  ITEM_ALREADY_EXIST: "Item already exist by this name. Enter a different name",
  DELETE_SUCCESS: "Successfully deleted item",
  ITEM_NOT_ADDED: "Could not add item. Please check the details you entered",
  ITEM_ADDED: "Successfully added item",
  INVALID_REQUEST: "Invalid request",
  ITEM_LIST_FETCHED: "Successfully fetched item list",
  ITEM_FETCHED: "Item fetched",
  INCORRECT_PASSWORD: "Incorrect password",
  USER_DOESNT_EXIST: "User doesn't exist",
  SUCCESS: "Success",
  USER_CREATED: "User created",
};

module.exports = {
  constants,
};
