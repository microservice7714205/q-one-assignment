const { constants } = require("../constants");
const { tokenForUser } = require("../helpers/jwt.helper");
const { respondSuccess, respondFailure } = require("../responseHandler");
const { getUser, createUser } = require("../services/user.service");
const { userValidator } = require("../validators");
const userController = require("../Controller/user.controller");

const userRouter = require("express").Router();

// Creates user with the given details
userRouter.post("/", userController.createuser);

// User login
userRouter.post("/login", userController.login);

module.exports = { userRouter };
