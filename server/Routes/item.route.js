const { ObjectId } = require("mongodb");
const { constants } = require("../constants");
const itemRouter = require("express").Router();

const itemController = require("../Controller/item.controller");

// Get item details by Item Id
itemRouter.get("/:itemId", itemController.getItemById);

// Get lists of all items
itemRouter.get("/", itemController.getItemList);

// Add an item
itemRouter.post("/add", itemController.addItem);

// Update item by item Id
itemRouter.put("/:itemId", itemController.updateItem);

// Delete item by item Id
itemRouter.delete("/:itemId", itemController.deleteItemById);

module.exports = { itemRouter };
