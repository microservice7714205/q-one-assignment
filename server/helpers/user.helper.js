const { getUser, createUser } = require("../services/user.service");

const createDummyUsersIfTheyDontExist = async () => {
  let users = await getUser();
  if (users) {
    return;
  }
  let userArray = [
    {
      username: "test1",
      password: "123456",
    },
    {
      username: "test2",
      password: "123456",
    },
    {
      username: "test3",
      password: "123456",
    },
  ];

  createUser(userArray);
};

module.exports = { createDummyUsersIfTheyDontExist };
