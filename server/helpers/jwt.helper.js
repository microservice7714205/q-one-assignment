const jwt = require("jsonwebtoken");

function tokenForUser(user) {
  const expire = process.env.JWT_EXPIRE;
  const options = expire ? { expiresIn: expire } : {};

  return jwt.sign(user, process.env.JWT_SECRET, options);
}

module.exports = { tokenForUser };
