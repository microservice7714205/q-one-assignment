const { dbClient } = require("../dbconfig");
const { constants } = require("../constants");
const User = dbClient.model(constants.user);
User.createIndex({ username: 1 });
module.exports = { User: User };
