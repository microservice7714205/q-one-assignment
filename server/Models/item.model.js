const { constants } = require("../constants");
const { dbClient } = require("../dbconfig");
const Item = dbClient.model(constants.item);
Item.createIndex({ itemName: 1 });
Item.createIndex({ state: 1 });

module.exports = { Item };
