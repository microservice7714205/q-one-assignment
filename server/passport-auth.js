const { getUser } = require("./services/user.service");
const ObjectId = require("mongodb").ObjectId;
var JwtStrategy = require("passport-jwt").Strategy,
  ExtractJwt = require("passport-jwt").ExtractJwt,
  passport = require("passport");
var opts = {
  jwtFromRequest: ExtractJwt.fromHeader("authorization"),
  secretOrKey: process.env.JWT_SECRET,
  passReqToCallback: true,
};

const jwtLogin = new JwtStrategy(opts, async (req, payload, done) => {
  let user = await getUser({ _id: new ObjectId(payload._id) });
  if (user) {
    return done(null, user);
  }
  const error = new Error("Please login");
  error.status = 401;
  return done(error, false);
});

passport.use(jwtLogin);

module.exports = {
  requireAuth: passport.authenticate("jwt", { session: false }),
};
