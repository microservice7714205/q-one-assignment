const { ObjectId } = require("mongodb");

class Modal {
  collectionInstance;

  constructor(collectionName, database) {
    this.collectionInstance = database.collection(collectionName);
  }

  createIndex(index) {
    if (Array.isArray(index)) {
      this.collectionInstance.createIndexes(index);
      return;
    }

    this.collectionInstance.createIndex(index);
  }

  save(data) {
    return this.collectionInstance.insertOne(data);
  }

  findOne(query) {
    return this.collectionInstance.findOne(query);
  }

  find() {
    return this.collectionInstance.find();
  }

  insertMany(array) {
    return this.collectionInstance.insertMany(array);
  }

  findOneAndUpdate(query, update) {
    return this.collectionInstance.findOneAndUpdate(query, update);
  }

  findOneAndDelete(query) {
    return this.collectionInstance.findOneAndDelete(query);
  }
}
module.exports = {
  Modal,
};
